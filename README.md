# AttackWireless2
ATTACKWIRELESS2 é um script escrito em python que pode escanear e atacar redes. É baseado em GUI, o que facilita a compreensão.

## O que posso fazer com isso?
### ESCANEAR
- Analise sua rede para hosts ativos, seu sistema operacional, portas abertas e muito mais.
- Procure pontos de acesso e descubra o tipo de criptografia, WPS e outros dados úteis.

### SPOOFING / SNIFFING
- Spoofing simples do ARP
- DNS Sniffing por ARP Spoofing o alvo e ouvindo DNS-Queries

### KICKING
- Kicking hospeda sua internet usando ARP-Spoof attack

### DESAUTENTICAR
- Enviar pacotes de autenticação para Pontos de Acesso em sua área (DoS)
- Derrubar Todos os Pontos de Acesso, basicamente faz o mesmo, mas examina redes e as ataca periodicamente.

## O que eu preciso? (Requisitos)
- Python
- Qualquer Distro Linux (Kali Linux se preferir )
- Os módulos necessários podem ser instalados na execução do script de forma prática caso não tenha os módulos.

<a href="http://pt-br.tinypic.com?ref=6hrgp3" target="_blank"><img src="http://i66.tinypic.com/6hrgp3.png" border="0" alt="Image and video hosting by TinyPic"></a>

### Modo de download e execução:
### git clone https://github.com/anonymouskillerbr1/AttackWireless.git
### cd AttackWireless
### unzip AttackWireless.zip
### cd AttackWireless
### chmod +x attackwireless.py
### sudo python attackwireless.py
### Obs: Deve-se usar o script em modo de super usuário para que não ocorra erros de permissão em algumas opções do script.

